//
//  AppDelegate.h
//  THTiPhone
//
//  Created by apple on 21/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>



@interface AppDelegate : UIResponder <UIApplicationDelegate>{
    
    UITabBarController *TabBarController;
    UIImageView *SplashImageView;
    UINavigationController *HomeNavigation;
    UINavigationController *SearchNavigation;
    UINavigationController *OpinionNavigation;
}
@property (nonatomic, retain) UINavigationController *HomeNavigation;
@property (nonatomic, retain) UINavigationController *SearchNavigation;
@property (nonatomic, retain) UINavigationController *OpinionNavigation;
@property (nonatomic, retain) UIWindow *window;
@property (nonatomic, retain) UITabBarController *TabBarController;
@property (nonatomic, retain) UIImageView *SplashImageView;

@end
