//
//  SearchViewController.h
//  THTiPhone
//
//  Created by apple on 17/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AdWhirlView.h"
@interface SearchViewController : UIViewController<AdWhirlDelegate> {
    
    AdWhirlView *adView;
}

@property (nonatomic, retain) AdWhirlView *adView;

@end
