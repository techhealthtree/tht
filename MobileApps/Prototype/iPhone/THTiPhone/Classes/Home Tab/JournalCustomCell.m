//
//  JournalCustomCell.m
//  THTJournal
//
//  Created by arun verma on 26/02/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "JournalCustomCell.h"

@implementation JournalCustomCell
@synthesize articleImage;
@synthesize articleTitle;
@synthesize articleDiscroption;

- (id)initWithStyle:(UITableViewCellStyle)style reuseIdentifier:(NSString *)reuseIdentifier
{
    self = [super initWithStyle:style reuseIdentifier:reuseIdentifier];
    if (self) {
        // Initialization code
    }
    return self;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated
{
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}



@end
