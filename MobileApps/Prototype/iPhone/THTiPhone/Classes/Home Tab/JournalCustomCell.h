//
//  JournalCustomCell.h
//  THTJournal
//
//  Created by Arun Verma on 26/02/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JournalCustomCell : UITableViewCell {
    
    UIImageView *articleImage;
    UILabel *articleTitle;
    UILabel *articleDiscroption;
    
}
@property (nonatomic, retain) IBOutlet UIImageView *articleImage;
@property (nonatomic, retain) IBOutlet UILabel *articleTitle;
@property (nonatomic, retain) IBOutlet UILabel *articleDiscroption;

@end
