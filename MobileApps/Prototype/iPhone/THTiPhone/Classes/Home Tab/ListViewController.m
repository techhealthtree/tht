//
//  ListViewController.m
//  THTiPhone
//
//  Created by apple on 17/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ListViewController.h"
#import "JournalCustomCell.h"
#import <QuickLook/QuickLook.h>
#import "CustomQLPreviewController.h"
@interface ListViewController ()

@end

@implementation ListViewController

@synthesize JournalListTable;
@synthesize JournalListArray;
@synthesize ImagesArray;
@synthesize DescArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.JournalListArray = [NSMutableArray array];
    NSArray *pdfs = [[NSBundle mainBundle] pathsForResourcesOfType:@"pdf" inDirectory:nil];
    
    [pdfs enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSString *str = (NSString *)obj;
        [self.JournalListArray addObject:str];
    }];
    
    self.imagesArray = [NSMutableArray array];
    NSArray *images = [[NSBundle mainBundle] pathsForResourcesOfType:@"jpeg" inDirectory:nil];
    
    [images enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        NSString *str = (NSString*)obj;
        [self.ImagesArray addObject:str];
    }];
    // NSLog(@"%@",self.imagesArray);
    [self loadDescriptionArray];
    
    self.JournalListTable.dataSource = self;
    self.JournalListTable.delegate = self;
    
    [self.JournalListTable reloadData];

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

#pragma mark-
#pragma mark Table View Delegate Methods

-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section;
{
    return [self.JournalListArray count];
}

- (float)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPat {
    return 100;
    
}
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    static NSString *CellIdentifier = @"JournalCustomCell";
    
    
    JournalCustomCell *cell = (JournalCustomCell*)[tableView dequeueReusableCellWithIdentifier:CellIdentifier];
    if (cell == nil) {
        
        NSArray *topLevelObjects = [[NSBundle mainBundle] loadNibNamed:@"JournalCustomCell" owner:self options:nil];
		
		for (id currentObject in topLevelObjects){
			if ([currentObject isKindOfClass:[UITableViewCell class]]){
				cell =  (JournalCustomCell *) currentObject;
				break;
			}
		}
    }
    
	cell.articleTitle.text =[[self.JournalListArray objectAtIndex:indexPath.row] lastPathComponent];
	cell.articleDiscroption.text = [self.DescArray objectAtIndex:indexPath.row];
    
    UIImage *image = [[UIImage alloc] initWithContentsOfFile:[self.ImagesArray objectAtIndex:indexPath.row]];
    cell.articleImage.image = image;
    [image release];
    
    return cell;
}

-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
         // When user taps a row, create the preview controller
       // QLPreviewController *previewer = [[[QLPreviewController alloc] init] autorelease];
        CustomQLPreviewController *previewer = [[[CustomQLPreviewController alloc] init] autorelease];

        // Set data source
        [previewer setDataSource:self];
        [previewer setDelegate:self];
        
        // Which item to preview
        [previewer setCurrentPreviewItemIndex:indexPath.row];
        [[self navigationController] pushViewController:previewer animated:YES];
    
}

#pragma mark -
#pragma mark Preview Controller

- (void)previewControllerWillDismiss:(QLPreviewController *)controller
{
    
}

-(void)previewControllerDidDismiss:(QLPreviewController *)controller
{
    
}

-(BOOL)previewController:(QLPreviewController *)controller shouldOpenURL:(NSURL *)url forPreviewItem:(id <QLPreviewItem>)item
{
    return YES;
}

- (NSInteger) numberOfPreviewItemsInPreviewController: (QLPreviewController *) controller 
{
	return [self.JournalListArray count];
}

- (id <QLPreviewItem>)previewController: (QLPreviewController *)controller previewItemAtIndex:(NSInteger)index 
{
    
    /*NSArray *fileComponents = [[arrayOfDocuments objectAtIndex: index] componentsSeparatedByString:@"."];
     
     NSString *path = [[NSBundle mainBundle] pathForResource:[fileComponents objectAtIndex:0] ofType:[fileComponents objectAtIndex:1]];
     */
	return [NSURL fileURLWithPath:[self.JournalListArray objectAtIndex:index]];
}

- (CGRect)previewController:(QLPreviewController *)controller frameForPreviewItem:(id <QLPreviewItem>)item inSourceView:(UIView **)view
{
    return CGRectMake(0, 0, 380, 320);
}

- (void)dealloc {
    
    [JournalListArray release];
    [JournalListTable release];
    [ImagesArray release];
    [DescArray release];
    
    [super dealloc];
}


- (void)loadDescriptionArray {
    
    NSMutableArray *temArray = [[NSMutableArray alloc] init];
    self.descArray = temArray;
    [temArray release];
    
    NSString *str = @"Interrupted Aortic Arch is a life threatening anomaly that has a ductus dependant circulation and needs urgent surgical intervention.";
    [self.DescArray addObject:str];
    str = @"We here by present a case of Type B Arch Interruption, presenting in adolescence, with an absent ductus treated by an off pump extraanatomic bypass and is a very rare case reported treated by the unique approach";
    [self.DescArray addObject:str];
    str = @"Aortic Arch interruption are a group of anomaly with a high incidence of mortality at an early age unless treated surgically.";
    [self.DescArray addObject:str];
    str = @"Aortic Arch interruption are a group of anomaly with a high incidence of mortality at an early age unless treated surgically.";
    [self.DescArray addObject:str];
    str = @"We here present a very rare incidence of Arch interruption with absent ductus and no other cardiac anomaly which was treated with an extraanatomic bypass in an off pump technique, and had a successful recovery. ";
    [self.DescArray addObject:str];
    str = @"The operation was a rare procedure in this type of case and is not presented in literature till date.";
    [self.DescArray addObject:str];
    
    
} 
@end
