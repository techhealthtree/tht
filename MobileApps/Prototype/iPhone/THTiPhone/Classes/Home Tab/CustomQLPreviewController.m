//
//  CustomQLPreviewController.m
//  THTiPhone
//
//  Created by apple on 18/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "CustomQLPreviewController.h"

@interface CustomQLPreviewController ()

@end

@implementation CustomQLPreviewController


- (void)viewDidLoad
{
    [super viewDidLoad];
    //self.navigationController.navigationBar.alpha = 0;
    
    UIView *v = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 320, 44)];
    [v setBackgroundColor:[UIColor whiteColor]];
    [self.navigationController.navigationBar addSubview:[v autorelease]];

    [UIView animateWithDuration:0.2 animations:^{
    } completion:^(BOOL finished) {
         [[self navigationItem] setRightBarButtonItem:nil];
        [v performSelectorInBackground:@selector(removeFromSuperview) withObject:nil];
    }];
}
-(void)viewDidAppear:(BOOL)animated{
    
    [[self navigationItem] setRightBarButtonItem:nil]; 
    //[[self navigationItem] setRightBarButtonItems:nil animated:NO];
}

- (void)addSome {
    
}
- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(UIInterfaceOrientation)interfaceOrientation
{
    return (interfaceOrientation == UIInterfaceOrientationPortrait);
}

@end
