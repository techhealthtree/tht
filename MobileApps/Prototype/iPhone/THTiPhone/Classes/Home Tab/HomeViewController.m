//
//  HomeViewController.m
//  THTiPhone
//
//  Created by apple on 17/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HomeViewController.h"
#import "ListViewController.h"
#import "AppDelegate.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

@synthesize DrugButton;
@synthesize JournalButton;
@synthesize adView;

#pragma mark - View lifecycle

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    AdWhirlView *adWhirlView = [AdWhirlView requestAdWhirlViewWithDelegate:self];
    adWhirlView.frame = CGRectMake(0, 381, 320, 50);
    [self.navigationController.view addSubview:adWhirlView];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


- (void)dealloc {
    
    [JournalButton release];
    [DrugButton release];
    [super dealloc];
}

// Implement viewDidLoad to do additional setup after loading the view, typically from a nib.

#pragma mark-
#pragma mark AdWhirl Method

- (NSString *)adWhirlApplicationKey {
	// return your SDK key  
	return kSampleAppKey;
	
}
- (UIViewController *)viewControllerForPresentingModalView {
	
	//return UIWindow.viewController;
	return [(AppDelegate *)[[UIApplication sharedApplication] delegate] HomeNavigation];
	
}
- (void)adWhirlDidReceiveAd:(AdWhirlView *)adWhirlView {
	
	[UIView beginAnimations:@"AdWhirlDelegate.adWhirlDidReceiveAd:"context:nil];
	
	[UIView setAnimationDuration:0.7];

	CGSize adSize = [adView actualAdSize];
	
	CGRect newFrame = adView.frame;
	
	newFrame.size = adSize;
	
	newFrame.origin.x = (self.view.bounds.size.width - adSize.width)/ 2;
	
	newFrame.origin.y=410;
    
	adView.frame = newFrame;
		
	[UIView commitAnimations];
	
}

#pragma mark-
#pragma mark Button Click Method

- (IBAction)drugButtonClicked:(id)sender {
    
} 

- (IBAction)journalButtonClicked:(id)sender {
    
    ListViewController *listView = nil;
    listView = [[ListViewController alloc] initWithNibName:@"ListViewController"
                                                            bundle:nil];
    
    [self.navigationController pushViewController:listView animated:YES];
    [listView release];
    listView = nil;
}

@end
