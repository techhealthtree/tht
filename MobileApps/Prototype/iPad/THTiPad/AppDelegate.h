//
//  AppDelegate.h
//  THTiPad
//
//  Created by apple on 18/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>


@interface AppDelegate : UIResponder <UIApplicationDelegate> {

    UIImageView *SplashImageView;
    UITabBarController *TabBarController;
    UINavigationController *HomeNavigation;
    UINavigationController *SearchNavigation;
    UINavigationController *OpinionNavigation;
}

@property (nonatomic,retain) UIWindow *window;
@property (nonatomic,retain) UIImageView *SplashImageView;
@property (nonatomic, retain) UITabBarController *TabBarController;
@property (nonatomic,retain) UINavigationController *HomeNavigation;
@property (nonatomic,retain) UINavigationController *SearchNavigation;
@property (nonatomic,retain) UINavigationController *OpinionNavigation;
@end

