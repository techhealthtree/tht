//
//  ListViewController.m
//  THTiPhone
//
//  Created by apple on 17/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "ListViewController.h"
#import "ReaderDocument.h"
#import "ReaderViewController.h"
#import "CustomView.h"
#import <QuartzCore/QuartzCore.h>



@implementation ListViewController

@synthesize ScrollView;
@synthesize JournalListArray;
@synthesize ImagesArray;
@synthesize DescArray;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    }
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.JournalListArray = [NSMutableArray array];
    NSArray *pdfs = [[NSBundle mainBundle] pathsForResourcesOfType:@"pdf" inDirectory:nil];
    
    [pdfs enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        NSString *str = (NSString *)obj;
        [self.JournalListArray addObject:str];
    }];
    
    self.imagesArray = [NSMutableArray array];
    NSArray *images = [[NSBundle mainBundle] pathsForResourcesOfType:@"jpeg" inDirectory:nil];
    
    [images enumerateObjectsUsingBlock:^(id obj, NSUInteger idx, BOOL *stop) {
        
        NSString *str = (NSString*)obj;
        [self.ImagesArray addObject:str];
    }];
    // NSLog(@"%@",self.imagesArray);
    [self loadDescriptionArray];
    [self loadScrollViewContentWithItem:[self.JournalListArray count]];
    

}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}

-(void)loadScrollViewContentWithItem:(int)items
{
    ScrollView.backgroundColor = [UIColor whiteColor];
    
    int x_axis = 20;
    int y_axis = 10;
    
    for (int i = 0; i < items ; i++) {
        if ((i != 0) && i % 2 == 0) {
            y_axis += 265;
            x_axis = 20;
        }
        
        CustomView *obj = [[CustomView alloc] initWithFrame:CGRectMake(x_axis, y_axis, 350, 260)
                                                                 andTitle:[self extractNameFormPath:[self.JournalListArray objectAtIndex:i]]
                                                                withImage:[self.ImagesArray objectAtIndex:i]
                                                           andDiscription:[self.DescArray objectAtIndex:i]];
        obj.layer.borderWidth = 2;
        obj.layer.borderColor = [UIColor lightGrayColor].CGColor;
        [obj setBackgroundColor:[UIColor clearColor]];
        obj.tag = 1000 + i;
        
        UITapGestureRecognizer *gesture = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(onArticleSelection:)];
        [obj addGestureRecognizer:gesture];
        [gesture release];
        
        [self.ScrollView addSubview:obj];
        [obj release];
        
        x_axis += 380;
        
        
    }
   
    [ScrollView setContentSize:CGSizeMake(768, y_axis+380)];
    [ScrollView scrollsToTop];
    
}
 
- (void)onArticleSelection:(UILongPressGestureRecognizer*)sender { 
    
    int ttag = sender.view.tag - 1000;
   
        NSString *filePath = [self.JournalListArray objectAtIndex:ttag];
        ReaderDocument *document = [ReaderDocument withDocumentFilePath:filePath password:nil];
        
        if (document != nil) 
        {
            ReaderViewController *readerViewController = [[ReaderViewController alloc] initWithReaderDocument:document];
            
            readerViewController.delegate = self;
            readerViewController.view.frame = self.view.frame;
            [self.navigationController pushViewController:readerViewController animated:YES];
            [readerViewController release];
        }
   
}
- (NSString*)extractNameFormPath:(NSString*)path {
    
    NSArray *array = [path componentsSeparatedByString:@"/"];
    return [array lastObject];
    
}


- (void)dealloc {
    
    [JournalListArray release];
    [ScrollView release];
    [ImagesArray release];
    [DescArray release];
    
    [super dealloc];
}


- (void)loadDescriptionArray {
    
    NSMutableArray *temArray = [[NSMutableArray alloc] init];
    self.descArray = temArray;
    [temArray release];
    
    NSString *str = @"A decision of surgical intervention by an extraanatomic bypass was planned. General anesthesia and a triple lumen central venous line and two arterial monitoring lines, one in the right radial and the other in the left femoral artery.";
    [self.DescArray addObject:str];
    str = @"A midline sternotomy was made and the thymus was dissected out. The pericardium was incised from the innominate vein superiorly to the diaphragm inferiorly. Horizontal extensions in an inverted T manner was made on both sides.";
    [self.DescArray addObject:str];
    str = @"Interrupted Aortic Arch with ductal involution is a rare occurrence in Type B interruption. Unlike the usual natural history this type of anomaly allows the child to grow to an adult stage due to the development of innumerable collaterals in the prenatal state.";
    [self.DescArray addObject:str];
    str = @" Surgical correction is necessary to correct proximal aortic hypertension, left ventricular failure and treatment of claudication [1]. Orthotopic correction in a grown up subject is usually not possible due to the presence of collaterals";
    [self.DescArray addObject:str];
    str = @"We here present a very rare incidence of Arch interruption with absent ductus and no other cardiac anomaly which was treated with an extraanatomic bypass in an off pump technique, and had a successful recovery. ";
    [self.DescArray addObject:str];
    str = @"Interrupted Aortic Arch is a life threatening anomaly that has a ductus dependant circulation and needs urgent surgical intervention. We here by present a case of Type B Arch Interruption, presenting in adolescence.";
    [self.DescArray addObject:str];
    
    
} 
@end
