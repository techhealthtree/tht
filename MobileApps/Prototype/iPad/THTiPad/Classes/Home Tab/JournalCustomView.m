//
//  JournalCustomView.m
//  THTJournal
//
//  Created by arun verma on 25/02/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "JournalCustomView.h"

@implementation JournalCustomView


- (id)initWithFrame:(CGRect)frame andTitle:(NSString*)title 
          withImage:(NSString*)imageStr 
     andDiscription:(NSString*)description {
    
    self = [super initWithFrame:frame];
    if (self) {
       
        UIImageView *imageView = [[UIImageView alloc] initWithFrame:CGRectMake(5, 5, 150, 100)];
        UIImage *image = [[UIImage alloc] initWithContentsOfFile:imageStr];
        imageView.image = image;
        [self addSubview:imageView];
        
        UILabel *journalName = [[UILabel alloc] initWithFrame:CGRectMake(160, 5, 180, 100)];
        [journalName setBackgroundColor:[UIColor clearColor]];
        journalName.text = title;
        journalName.numberOfLines = 5;
        [journalName setFont:[UIFont boldSystemFontOfSize:20]];
        
        UILabel *journalDiscription = [[UILabel alloc] initWithFrame:CGRectMake(5,120, 340, 150)];
        [journalDiscription setBackgroundColor:[UIColor clearColor]];
        journalDiscription.numberOfLines = 8;
        [journalDiscription setFont:[UIFont systemFontOfSize:17]];
        journalDiscription.text= description;
        
        [self addSubview:journalName];
        [self addSubview:journalDiscription];
        
        [journalName release];
        [journalDiscription release];
        [image release];

    }
    return self;
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect
{
    // Drawing code
}
*/

@end
