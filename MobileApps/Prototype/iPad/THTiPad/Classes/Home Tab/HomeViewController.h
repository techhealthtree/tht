//
//  HomeViewController.h
//  THTiPhone
//
//  Created by apple on 17/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AdWhirlView.h"
@interface HomeViewController : UIViewController<AdWhirlDelegate> {
    
    AdWhirlView *adView;

    UIButton *DrugButton;
    UIButton *JournalButton;
   
}

@property (retain, nonatomic) IBOutlet UIButton *DrugButton;
@property (retain, nonatomic) IBOutlet UIButton *JournalButton;
@property (nonatomic, retain) AdWhirlView *adView;

- (IBAction)drugButtonClicked:(id)sender;
- (IBAction)journalButtonClicked:(id)sender;
@end
