//
//  CustomView.h
//  THTiPad
//
//  Created by apple on 18/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomView : UIView
- (id)initWithFrame:(CGRect)frame andTitle:(NSString*)title 
          withImage:(NSString*)imageStr 
     andDiscription:(NSString*)description;
@end
