//
//  HomeViewController.m
//  THTiPhone
//
//  Created by apple on 17/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "HomeViewController.h"
#import "ListViewController.h"
#import "AppDelegate.h"
#import "AdController.h"
@interface HomeViewController ()

@end

@implementation HomeViewController

@synthesize DrugButton;
@synthesize JournalButton;
@synthesize adView;

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil];
    if (self) {
        // Custom initialization
    } 
    return self;
}

- (void)viewDidLoad
{
    [super viewDidLoad];
   /* AdWhirlView *adWhirlView = [AdWhirlView requestAdWhirlViewWithDelegate:self];
    UIImageView *adImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 880, 640, 100)];
    adWhirlView.frame = CGRectMake(0, 880, 640, 100);
    
    UIImage *adImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"medhire_iPad" ofType:@"png"]];
    adImageView.image = adImage;
    [adImage release];
    
    [adWhirlView addSubview:adImageView];

    [self.navigationController.view addSubview:adWhirlView];
  */ 
    AdController *controller = [[AdController alloc] initWithNavigationController:self.navigationController];
    //controller.NavigationController = self.navigationController;
    //[self.navigationController.view addSubview:controller];
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    // Release any retained subviews of the main view.
    // e.g. self.myOutlet = nil;
}


#pragma mark-
#pragma mark Button Click Method

-(IBAction)drugButtonClicked:(id)sender {
    
}

-(IBAction)journalButtonClicked:(id)sender {
    
    ListViewController *listView = nil;
    listView = [[ListViewController alloc] initWithNibName:@"ListViewController"
                                                            bundle:nil];
    
    [self.navigationController pushViewController:listView animated:YES];
    [listView release];
    listView = nil;

}
#pragma mark-
#pragma mark AdWhirl Method
/*
- (NSString *)adWhirlApplicationKey {
	// return your SDK key  
	return kSampleAppKey;
	
}
- (UIViewController *)viewControllerForPresentingModalView {
	
	//return UIWindow.viewController;
	return [(AppDelegate *)[[UIApplication sharedApplication] delegate] OpinionNavigation];
	
}
- (void)adWhirlDidReceiveAd:(AdWhirlView *)adWhirlView {
	
	[UIView beginAnimations:@"AdWhirlDelegate.adWhirlDidReceiveAd:"context:nil];

	[UIView setAnimationDuration:0.7];
    
	CGSize adSize = [adView actualAdSize];
	
	CGRect newFrame = adView.frame;
	
	newFrame.size = adSize;
	
	//newFrame.origin.x = (self.view.bounds.size.width - adSize.width)/ 2;
	//newFrame.size.width = 680;
	//newFrame.origin.y=410;
    newFrame = CGRectMake(0, 880, 640, 100);
	adView.frame = newFrame;
    NSLog(@"%@",NSStringFromCGSize(adSize));
	[UIView commitAnimations];

}
*/
#pragma mark - View lifecycle

- (void)dealloc {
    [JournalButton release];
    
    [DrugButton release];
    [super dealloc];
}



@end
