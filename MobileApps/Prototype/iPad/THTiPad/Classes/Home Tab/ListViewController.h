//
//  ListViewController.h
//  THTiPhone
//
//  Created by apple on 17/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ListViewController : UIViewController {
    
    UIScrollView *ScrollView;
    
    NSMutableArray *JournalListArray;
    NSMutableArray *ImagesArray;
    NSMutableArray *DescArray;
}
@property (retain, nonatomic) IBOutlet UIScrollView *ScrollView;
@property (retain, nonatomic) NSMutableArray *JournalListArray;
@property (retain, nonatomic) NSMutableArray *ImagesArray;
@property (retain, nonatomic) NSMutableArray *DescArray;

- (void)loadDescriptionArray;
@end
