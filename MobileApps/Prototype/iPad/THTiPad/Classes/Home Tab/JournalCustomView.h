//
//  JournalCustomView.h
//  THTJournal
//
//  Created by arun verma on 25/02/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface JournalCustomView : UIView {
  
}


- (id)initWithFrame:(CGRect)frame andTitle:(NSString*)title 
                                    withImage:(NSString*)imageStr 
                                andDiscription:(NSString*)description;

@end
