#import <UIKit/UIKit.h>
#import "AdWhirlDelegateProtocol.h"
#import "AdWhirlView.h"


@class HomeViewController;

@interface AdController : NSObject <AdWhirlDelegate>

{
	CGSize screenSize;
	AdWhirlView *adView;
    UIButton *adBanner;
	HomeViewController *viewController;
	BOOL bannerIsVisible;
    UINavigationController *NavigationController;
}

@property (nonatomic,assign) BOOL bannerIsVisible;
@property(nonatomic,retain) AdWhirlView *adView;
@property (nonatomic, retain) UINavigationController *NavigationController;
-(void)showPersonalAd;
-(void)openURLforAd;

- (void) resizeAd;
- (void) animateOnScreen;
- (void) animateOffScreen;

@end
