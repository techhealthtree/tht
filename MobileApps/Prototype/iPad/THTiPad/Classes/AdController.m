// Import the interfaces
#import "AdController.h"

//Same assumption as before, remember?
#import "AdWhirlView.h"
#import "AppDelegate.h"
#import "HomeViewController.h"

#define kAdViewWidth        320
#define kAdViewHeight       50
#define kIpadAdViewWidth    768
#define kIpadAdViewHeight   110

@implementation AdController

@synthesize adView;
@synthesize bannerIsVisible;
@synthesize NavigationController;

// on "init" you need to initialize your instance
-(id) initWithNavigationController:(UINavigationController*) controller
{
	// always call "super" init
	// Apple recommends to re-assign "self" with the "super" return value
	if( (self=[super init] )) 
	{
        NavigationController = controller;
		// ask director the the window size
		screenSize = CGSizeMake([[UIScreen mainScreen] bounds].size.width, [[UIScreen mainScreen] bounds].size.height);
        
        //Assign the AdWhirl Delegate to our adView
        self.adView = [AdWhirlView requestAdWhirlViewWithDelegate:self];
        //This is required, even if you don't want to :P
        self.adView.autoresizingMask = UIViewAutoresizingFlexibleLeftMargin|UIViewAutoresizingFlexibleRightMargin;
        //This isn't really needed but also it makes no harm. It just retrieves the configuration
        //from adwhirl.com so it knows what Ad networks to use
        [adView updateAdWhirlConfig];
       
        //self.adView.frame = CGRectMake((screenSize.width/2)-(kAdViewWidth/2),screenSize.height + kAdViewHeight,kAdViewWidth,kAdViewHeight);
        self.adView.frame = CGRectMake(60, 880, 640, 100);
        //self.adView.clipsToBounds = YES;
        //Adding the adView (used for our Ads) to the rootViewController
        
        //AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        // viewController = delegate.viewController;
        //[[delegate.window.rootViewController view] addSubview:adView];
        [controller.view addSubview:adView];
        
		[self showPersonalAd];
	}
	return self;
}

-(void)showPersonalAd 
{
    
   // AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];

    
    if (!adBanner)
    {
        UIImage *adBannerImage;
        
         //This will create an Ad for anything you want, just replace the image with your own
        if (UI_USER_INTERFACE_IDIOM() == UIUserInterfaceIdiomPad)
        {
            adBannerImage = [UIImage imageNamed:@"medhire_iPad"];
        }
        else
        {
            adBannerImage = [UIImage imageNamed:@"medhire_iPad"];
        }
        
        adBanner = [UIButton buttonWithType: UIButtonTypeCustom];
    
       // adBanner.frame = CGRectMake(0,delegate.window.rootViewController.view.frame.size.height + adBannerImage.size.height, adBannerImage.size.width, adBannerImage.size.height);
         adBanner.frame = CGRectMake(60, 880, 640, 100);
        [adBanner setImage: adBannerImage
                  forState: UIControlStateNormal];
        [adBanner setImage: adBannerImage 
                  forState: UIControlStateSelected];
        
        [adBanner addTarget:self 
                     action:@selector(openURLforAd) 
           forControlEvents:UIControlEventTouchDown];
        
        // [delegate.window.rootViewController.view addSubview: adBanner];
        [NavigationController.view addSubview:adBanner];
    }
  
    [UIView beginAnimations:@"animatePersonalAdOn" context:NULL];
	//adBanner.frame = CGRectMake(0, delegate.window.rootViewController.view.frame.size.height - adBanner.frame.size.height, adBanner.frame.size.width, adBanner.frame.size.height);
     self.adView.frame = CGRectMake(60, 880, 640, 100);
	[UIView commitAnimations];
}

-(void)openURLforAd 
{
    NSURL *url=[NSURL URLWithString:@"http://www.medhire.asia"];
	[[UIApplication sharedApplication] openURL:url];
}

- (void) resizeAd
{
   // AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    
	[UIView beginAnimations:@"AdResize" context:nil]; 
	[UIView setAnimationDuration:0.7]; 
	CGSize adSize = [adView actualAdSize];
    CGRect newFrame = adView.frame;
	newFrame.size.height = adSize.height; // fit the ad 
	newFrame.size.width = adSize.width; 
	newFrame.origin.x = (screenSize.width - adSize.width)/2; // center 
	if (self.bannerIsVisible)
	{
		//newFrame.origin.y = delegate.window.rootViewController.view.frame.size.height - adSize.height;
        
	}
	else
	{
		//newFrame.origin.y = delegate.window.rootViewController.view.frame.size.height + adSize.height;
	}
	adView.frame = newFrame; 
	 self.adView.frame = CGRectMake(60, 880, 640, 100);
	// ... adjust surrounding views here ... 
	[UIView commitAnimations];
    
	
}

- (void) animateOnScreen
{
	if (!self.bannerIsVisible)
	{
       // AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
        //[[delegate.window.rootViewController view] bringSubviewToFront: adView]; //make sure the ads don't get covered up.
        
		[UIView beginAnimations:@"animateAdBannerOn" context:NULL];
		// banner is invisible now and moved out of the screen on adSize.height px
		CGSize adSize = [adView actualAdSize];
        //adView.frame = CGRectMake((screenSize.width/2)-(adSize.width/2),delegate.window.rootViewController.view.frame.size.height - adSize.height, adSize.width, adSize.height);
         self.adView.frame = CGRectMake(60, 880, 640, 100);
        adBanner.frame = CGRectOffset(adBanner.frame, 0, adBanner.frame.size.height);//move personal ad off screen
         
        [UIView commitAnimations];
		self.bannerIsVisible = YES;
	}
}

- (void) animateOffScreen
{
	[UIView beginAnimations:@"animateAdBannerOff" context:NULL];
	// banner is visible and we move it out of the screen, due to connection issue
	// I chose to move it off the side of the screen so that that it will work well, 
	// whether it is at the top or the bottom of the screen
	CGSize adSize = [adView actualAdSize];
	adView.frame = CGRectOffset(adView.frame, 0, 1.5 * adSize.height);
	[UIView commitAnimations];
	self.bannerIsVisible = NO;
    
    [self showPersonalAd];
    
}

// on "dealloc" you need to release all your retained objects
- (void) dealloc
{
	// in case you have something to dealloc, do it in this method
    
    if (adView) 
    {
        //Remove adView from superView
        [adView removeFromSuperview];
        //Replace adView's view with "nil"
        [adView replaceBannerViewWith:nil];
        //Tell AdWhirl to stop requesting Ads
        [adView ignoreNewAdRequests];
        //Set adView delegate to "nil"
        [adView setDelegate:nil];
        //Release adView
        [adView release];
    }
	
    //Remove the adView controller
    self.adView.delegate = nil;
    self.adView = nil;
    
	// don't forget to call "super dealloc"
	[super dealloc];
}


//These are the methods for the AdWhirl Delegate, you have to implement them
#pragma mark AdWhirlDelegate methods

- (void)adWhirlWillPresentFullScreenModal
{
    //pause stuff here
}

- (void)adWhirlDidDismissFullScreenModal {
    //Once the user closes the Ad he'll want to return to the game and continue where
    //he left it
    //[self resume];
}

- (NSString *)adWhirlApplicationKey 
{
	NSString * key;
    
    key = [NSString stringWithFormat: @"69d701f5f83445deac37452e6c793954"];    // iphone
	
	return key;
}

- (NSString *)admobPublisherID
{
    NSString *admobID = [NSString stringWithFormat:@"a14e09d326734f7"];
    
    self.adView.frame = CGRectMake(100, 880, 640, 100);
    
    return admobID;
}


- (NSString *)MdotMApplicationKey
{
    NSString *key = [NSString stringWithFormat:@"cd387cb73afc16b7d662e1a89313b084"];
    
    if ([key isEqualToString: [NSString stringWithFormat:@"cd387cb73afc16b7d662e1a89313b084"]]) 
    {
        NSLog(@"WARNING: Change the MdotMApplicationKey to your own!!!");
    }
    
    return key;
}

- (BOOL)adWhirlTestMode
{
	return NO;
}


- (UIViewController *)viewControllerForPresentingModalView {
    //Remember that UIViewController we created in the Game.h file? AdMob will use it.
    //If you want to use "return self;" instead, AdMob will cancel the Ad requests.
     AppDelegate *delegate = (AppDelegate*)[[UIApplication sharedApplication] delegate];
    return delegate.window.rootViewController;
}

-(void)adWhirlDidReceiveAd:(AdWhirlView *)adWhirlView 
{
	[self resizeAd];
	[self animateOnScreen];
}

-(void)adWhirlDidFailToReceiveAd:(AdWhirlView *)adWhirlView usingBackup:(BOOL)yesOrNo {
    //The code to show my own Ad banner again
	//For our example, it will be this method
	//You can do any kind of validation in here, for more info please check the AdWhirl documentation
	if (self.bannerIsVisible && !yesOrNo)
	{
		[self animateOffScreen];
	}
    
}

@end