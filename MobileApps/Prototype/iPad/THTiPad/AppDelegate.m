//
//  AppDelegate.m
//  THTiPhone
//
//  Created by Arun Verma on 17/03/12.
//  Copyright (c) 2012 __MyCompanyName__. All rights reserved.
//

#import "AppDelegate.h"
#import "HomeViewController.h"
#import "SearchViewController.h"
#import "OpinionViewController.h"


@implementation AppDelegate

@synthesize window = _window;
@synthesize TabBarController;
@synthesize SplashImageView;
@synthesize OpinionNavigation;
@synthesize SearchNavigation;
@synthesize HomeNavigation;

- (void)dealloc
{
    [_window release];
    [TabBarController release];
    [SplashImageView release];
    [HomeNavigation release];
    [SearchNavigation release];
    [OpinionNavigation release];
    [super dealloc];
}

- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions
{
    //for increasing the splash screen time
    sleep(2);
    self.window = [[[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]] autorelease];
    self.SplashImageView = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, 768, 1024)];
    //adding tabbar controller...
    [self buildTabBarController];
    
    self.window.rootViewController = self.TabBarController;
    [self.window makeKeyAndVisible];
    return YES;
}
-(void) buildTabBarController {
    
    TabBarController = nil;
    HomeViewController *homeObject = nil;
    SearchViewController *searchObject = nil;
    OpinionViewController *opinionObject = nil;
    
    homeObject = [[HomeViewController alloc] initWithNibName:@"HomeViewController"
                                                      bundle:nil];
    searchObject = [[SearchViewController alloc] initWithNibName:@"SearchViewController"
                                                          bundle:nil];
    opinionObject = [[OpinionViewController alloc] initWithNibName:@"OpinionViewController"
                                                            bundle:nil];
    
    HomeNavigation = [[UINavigationController alloc] initWithRootViewController:homeObject];
    SearchNavigation = [[UINavigationController alloc] initWithRootViewController:searchObject];
    OpinionNavigation = [[UINavigationController alloc] initWithRootViewController:opinionObject];
    
    HomeNavigation.title = @"Home";
    SearchNavigation.title = @"Search";
    OpinionNavigation.title = @"Opinion";
    
    TabBarController = [[UITabBarController alloc] init];
    [TabBarController setViewControllers:[NSArray arrayWithObjects:HomeNavigation,SearchNavigation,OpinionNavigation,nil] animated:YES];
    
    [homeObject release];
    [searchObject release];
    [opinionObject release];
    
}

- (void)applicationWillResignActive:(UIApplication *)application
{
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application
{
    [self.TabBarController setSelectedIndex:0];
    
    UINavigationController *navigationController = [[self.TabBarController viewControllers] objectAtIndex:0];
    
    if ([[navigationController viewControllers] count] > 1) {
        [navigationController popToViewController:[[navigationController viewControllers] objectAtIndex:1] animated:NO];
    }
    
    UIImage *splashImage = [[UIImage alloc] initWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"Default" ofType:@"png"]];
    self.SplashImageView.image = splashImage;
    [self.window addSubview:SplashImageView];
    
    [splashImage release];
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application
{
    [self performSelector:@selector(removeSplashView) withObject:nil afterDelay:2];
}
- (void)removeSplashView {
    [self.SplashImageView removeFromSuperview];
}
- (void)applicationDidBecomeActive:(UIApplication *)application
{
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
}

- (void)applicationWillTerminate:(UIApplication *)application
{
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
}

@end
